from setuptools import find_packages, setup


install_requires = [
    'sanic==19.3.1',
    'aiohttp',
]
# pip install -e '.[test]'
test_requires = [
    'pytest',
    'pytest-asyncio',
    'pytest-cov',
]

setup(
    name='sanic_demo',
    version='1.0.0',
    license='Proprietary',
    packages=find_packages(exclude=['tests.*', 'tests']),
    zip_safe=False,
    install_requires=install_requires,
    platforms='any',
    extras_require={
        'test': test_requires,
    }
)
