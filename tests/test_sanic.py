from sanic_demo.app import app


def test_client():
    client = app.test_client

    _, response = client.get('/')
    assert response.json == {'hello': 'world'}
